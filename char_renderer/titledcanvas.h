#ifndef TITLED_H
#define TITLED_H
#include"canvas.h"
#include<string>
class TitledCanvas:public Canvas{


public:
	TitledCanvas(int width, int height, std::string name);
	void setChar(int i, int j, char ch) override;
	~TitledCanvas();

};
#endif
