#ifndef CANVAS_H
#define CANVAS_H

#include<vector>
#include<iostream>
#include<ostream>

using namespace std;


class Canvas{

protected:

	vector<vector<char>> canv;
	vector<vector<bool>> client;
	
	void init(int , int, char);
	Canvas(int, int);

	Canvas(int, int, char);
public:



	~Canvas();

	virtual	void setChar(int, int, char);
	void clear(char);
	void clear();
	char getChar(int,int) const;
	
	int getHeight() const;
	
	int getWidth()  const;
	
	friend ostream& operator<<(ostream&, const Canvas&);

};

#endif
