#ifndef RHOMBUS_H
#define RHOMBUS_H
#include"shape.h"
class Rhombus:
	public Shape{

public:
	Rhombus(int);
	Rhombus(int, string);

	float area() const override;
	int areaScreen() const override;
	float perm() const override;
	int permScreen() const override;
	float bbw() const override;
	float bbh() const override;
	void scale(int);
	void draw(int, int, Canvas&, char) override;
	

    

	
	~Rhombus(){}

};
#endif
