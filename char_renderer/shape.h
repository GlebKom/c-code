#ifndef SHAPE_H
#define SHAPE_H

#include"canvas.h"
#include<math.h>
#include<typeinfo>
#include<string>
#include<sstream>

using namespace std;

class Shape{

protected:

	static size_t ID;
	
	size_t localID;
	
	int h, w;
	
	string name, desc;

	Shape(int, int);

	~Shape();

public:
		
	virtual float area() const =0; // returns shape area
	virtual float perm() const =0; // returns shape perimeter
	virtual float bbw() const = 0; // bounding box width
	virtual float bbh() const = 0; // bounidng box height
	
	virtual int areaScreen() const =0;
	virtual int permScreen() const =0;
	
	virtual void draw(int, int, Canvas&, char)=0;
	virtual void scale(int)=0;
	void setDesc(string);
	string getName() const;
	string getDesc() const;
	size_t getID() const;
	string toString();
	friend ostream& operator<<(ostream&, const Shape&);


};
#endif




