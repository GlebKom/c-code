#include"rightangle.h"

RightAngle::RightAngle(int height) :Triangle(height, height){ name = "RightAngle"; desc = "Generic RightAngle"; }

RightAngle::RightAngle(int height, string thatName) :Triangle(height, height){ name = thatName; desc = "Generic RightAngle"; }

float RightAngle::perm() const  { return h*(2 + sqrt(2)); }

void RightAngle::scale(int x)  { if (h + x >0){ h += x; w += x; } }
int RightAngle::areaScreen() const  { return (h*(h + 1)) / 2; }
int RightAngle::permScreen() const  { return 3 * (h - 1); }

void RightAngle::draw(int x0, int y0, Canvas& can, char ch)  {
	for (int i = y0; i < (h + y0) && i < can.getHeight(); ++i){
		for (int j = x0; j <= (i - y0 + x0) && j<can.getWidth(); ++j){
			if (i>=0 && j>=0)
				can.setChar(i, j, ch);

		}

	}
}