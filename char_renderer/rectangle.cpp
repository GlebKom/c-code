#include "rectangle.h"	


Rectangle::Rectangle(int height, int width): Shape(width,height){name="Rectangle"; desc="Generic Rectangle";}

Rectangle::Rectangle(int height, int width, string thatName): Shape(width,height){name=thatName; desc="Generic Rectangle";}

Rectangle::~Rectangle(){};

float Rectangle::area() const {return h * w;}
	

float Rectangle::perm() const {return 2*(h+w);}
	

float Rectangle::bbw() const {return w;}


float Rectangle::bbh() const {return h;}

void Rectangle::scale(int x) {if((h+x)>0 && (w+x)>0){w+=x;h+=x;}}





int Rectangle::areaScreen() const {return h*w;}

int Rectangle::permScreen() const {return 2*(h+w) - 4;}


void Rectangle::draw(int x0, int y0, Canvas& can, char ch) {

		for(int i = y0; i < (y0 + h) && i < can.getHeight();++i){
		
			for(int j = x0; j < (x0 + w) && j<can.getWidth();++j){
				if (i>=0 && j>=0)
					can.setChar(i, j, ch);

				}
		}
	}