#include"rhombus.h"

void Rhombus::draw(int x0, int y0, Canvas& can, char ch)  {
	int left, right;

	x0 += bbw() / 2;

	for (int i = 0; i < h / 2; ++i){
		if (y0 + i >= 0 && y0 + i < can.getHeight()){

			left = x0 - i;
			right = x0 + i;

			for (int tmp = left; tmp <= right; ++tmp){
				if (tmp>=0 && tmp<can.getWidth())
					can.setChar(y0 + i, tmp, ch);
				}
			}
		}
	
	int k = 0;

	for (int i = h - 1; i >= h / 2; --i, k++){
		if (y0 + i >= 0 && y0 + i < can.getHeight()){
			left = x0 - k;
			right = x0 + k;
			for (int tmp = left; tmp <= right; ++tmp){
				if (tmp>=0 && tmp<can.getWidth())
					can.setChar(y0 + i, tmp, ch);
			}
		}
	}

}

int Rhombus::areaScreen() const { int n = h / 2; return 2 * n*(n + 1) + 1; }

int Rhombus::permScreen() const { return 2 * (h - 1); }

void Rhombus::scale(int x){

	if (h % abs(x) == 0 && h + x > 1){
		h += x;

	}
}

float Rhombus::area() const  { return h*h / 2.0F; }
float Rhombus::perm() const  { return 2 * sqrt(2) * h; }


float Rhombus::bbw() const  { return h; }

float Rhombus::bbh() const  { return h; }

Rhombus::Rhombus(int height) : Shape((height % 2 == 0) ? (height + 1) : height, (height % 2 == 0) ? (height + 1) : height){ name = "Rhombus"; desc = "Generic Rhombus"; }

Rhombus::Rhombus(int height, string thatName) : Shape((height % 2 == 0) ? (height + 1) : height, (height % 2 == 0) ? (height + 1) : height){ name = thatName; desc = "Generic Rhombus"; }