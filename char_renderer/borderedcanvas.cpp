#include"borderedcanvas.h"
BorderedCanvas::BorderedCanvas(int width, int height) :Canvas(width + 2, height + 2){
	client[0][0] = false;
	canv[0][0] = '+';

	for (size_t i = 1; i < client[0].size() - 1; ++i){
		client[0][i] = false;
		canv[0][i] = '-';
	}
	client[client.size() - 1][client[0].size() - 1] = false;
	canv[client.size() - 1][client[0].size() - 1] = '+';

	client[client.size() - 1][0] = false;
	canv[client.size() - 1][0] = '+';

	for (size_t i = 1; i < client[0].size() - 1; ++i){
		client[client.size() - 1][i] = false;
		canv[client.size() - 1][i] = '-';
	}
	client[0][client[0].size() - 1] = false;
	canv[0][client[0].size() - 1] = '+';

	for (size_t i = 1; i < client.size() - 1; ++i){
		client[i][0] = false;
		canv[i][0] = '|';
		client[i][client[i].size() - 1] = false;
		canv[i][client[i].size() - 1] = '|';
	}




}
void BorderedCanvas::setChar(int i, int j, char ch)  { if ((i + 1)<getHeight() && (j + 1)<getWidth() && client[i + 1][j + 1]) canv[i + 1][j + 1] = ch; }
BorderedCanvas::~BorderedCanvas(){}