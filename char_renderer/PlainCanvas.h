#ifndef PLAINCANVAS_H
#define PLAINCANVAS_H
#include "canvas.h"
class PlainCanvas :
	public Canvas
{
public:
	PlainCanvas(int, int);
	PlainCanvas(int, int, char);
	~PlainCanvas();
};
#endif

