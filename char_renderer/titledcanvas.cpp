#include"titledcanvas.h"

void TitledCanvas::setChar(int i, int j, char ch)  { if ((i + 3)<getHeight() && (j + 1)<getWidth() && client[i + 3][j + 1]) canv[i + 3][j + 1] = ch; }



TitledCanvas::TitledCanvas(int width, int height, string name) :Canvas(width + 2, height + 4){

	client[0][0] = false;
	canv[0][0] = '+';

	for (size_t i = 1; i < client[0].size() - 1; ++i){
		client[0][i] = false;
		canv[0][i] = '-';
	}
	client[client.size() - 1][client[0].size() - 1] = false;
	canv[client.size() - 1][client[0].size() - 1] = '+';

	client[client.size() - 1][0] = false;
	canv[client.size() - 1][0] = '+';

	for (size_t i = 1; i < client[0].size() - 1; ++i){
		client[client.size() - 1][i] = false;
		canv[client.size() - 1][i] = '-';
	}
	client[0][client[0].size() - 1] = false;
	canv[0][client[0].size() - 1] = '+';

	for (size_t i = 1; i < client.size() - 1; ++i){
		client[i][0] = false;
		canv[i][0] = '|';
		client[i][client[i].size() - 1] = false;
		canv[i][client[i].size() - 1] = '|';
	}

	for (size_t i = 1; i < canv[1].size() - 1; ++i){
		if (i - 1 < name.size()) canv[1][i] = name[i - 1];
		else canv[1][i] = ' ';
		client[1][i] = false;
	}
	for (size_t i = 1; i < canv[2].size() - 1; ++i){

		canv[2][i] = '-';
		client[2][i] = false;
	}


}

TitledCanvas::~TitledCanvas(){}