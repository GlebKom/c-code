#include "canvas.h"
	
	Canvas::Canvas(int width, int height){init(width, height,' ');}
	
	Canvas::Canvas(int width, int height, char ch){init(width, height, ch);}
	
	Canvas::~Canvas(){}

	void Canvas::setChar(int i, int j, char ch){canv[i][j] = ch;}
	
	char Canvas::getChar(int i, int j) const {return canv[i][j];}
	
	int Canvas::getHeight() const {return canv.size();}
	
	int Canvas::getWidth()  const {return canv[0].size();}
		
	ostream& operator<<(ostream& out, const Canvas& can){
		
		for(int i = 0; i < can.getHeight(); ++i){
		
			for(int j = 0; j < can.getWidth();++j)
				
				out<<can.getChar(i, j);
				
				out<<"\n";
		
		}
		
		out<<"\n";
					
		return out;
	}

	void Canvas::clear(char ch){
		for (size_t i = 0; i < canv.size(); ++i)

			for (size_t j = 0; j < canv[i].size(); ++j)

				setChar(i, j, ch);
	}

	void Canvas::clear(){ clear(' ');}


	void Canvas::init(int width, int height, char ch){

	canv.resize(height);

	for(size_t i = 0; i <canv.size(); ++i)
	
	canv[i].resize(width);	
		

	for(size_t i = 0; i < canv.size(); ++i)
	
		for(size_t j = 0; j < canv[i].size();++j)
		
			canv[i][j] = ch;
	
	client.resize(height);
	for (size_t i = 0; i< client.size(); ++i)
		client[i].resize(width);


	for (size_t i = 0; i<client.size(); ++i)
		for (size_t j = 0; j<client[i].size(); ++j)
			client[i][j] = true;




	}