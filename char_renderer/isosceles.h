#ifndef ISOSCALES_H
#define ISOSCALES_H

#include"triangle.h"

class Isosceles: public Triangle{
	
public:

Isosceles(int);

Isosceles(int, string);



int areaScreen() const override;

int permScreen() const override;

void scale(int) override;

void draw(int, int, Canvas&, char) override;

float perm() const override;    
~Isosceles();
};

#endif