#ifndef TRIANGLE_H
#define TRIANGLE_H
#include "shape.h"




class Triangle: public Shape {

protected:

	Triangle(int height, int width);


public:


	float bbh() const override;
	float bbw() const override;
	float area() const override;
	~Triangle();


};

#endif