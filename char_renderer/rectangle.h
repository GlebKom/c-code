#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "shape.h"

class Rectangle:public Shape{

public:
	
	Rectangle(int, int);

	Rectangle(int, int, string);



	int areaScreen() const override;
	float area() const override;
	int permScreen() const override;
	float perm() const override;
	float bbw() const override;
	float bbh() const override;

	void scale(int) override;
	
	void draw(int, int, Canvas& , char) override;
	~Rectangle();
};
#endif
