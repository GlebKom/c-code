#include "isosceles.h"


Isosceles::Isosceles(int height): Triangle(height,2*height-1){name="Isosceles"; desc="Generic Isosceles";}


Isosceles::Isosceles(int height, string thatName): Triangle(height,2*height-1){name=thatName; desc="Generic Isosceles";}

Isosceles::~Isosceles(){}

int Isosceles::areaScreen() const {return h*h;}

int Isosceles::permScreen() const {return 4*(h-1);}

void Isosceles::scale(int x) {if (h + x > 0) {h+=x;w=2*h - 1;}}

float Isosceles::perm() const {return w + 2.0F * sqrtf(0.25F * w * w + h * h);}

void Isosceles::draw(int x0, int y0, Canvas& can, char ch) {
	
	int left, right;
	x0 += bbw()/2;
	for(int i = 0; i < h; ++i){
	
		left =	x0 - i;
		right = x0 + i;
		if (y0 + i >= 0 && y0 + i < can.getHeight()){
			for (int tmp = left; tmp <= right; ++tmp){
				if (tmp >= 0 && tmp < can.getWidth())
					can.setChar(y0 + i, tmp, ch);
				}
			}
		}
	}
