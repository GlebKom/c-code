#ifndef RIGHTANGLE_H
#define RIGHTANGLE_H


#include"triangle.h"

class RightAngle:public Triangle{

public:
	
	
	RightAngle(int height);

	RightAngle(int height, string thatName);
	
	float perm() const override;
	int permScreen() const override;
	void draw(int posx, int posy, Canvas& can, char ch) override;
	void scale(int x) override;
	int areaScreen() const override;

	~RightAngle(){}


};



#endif
