#include"shape.h"



Shape::Shape(int height, int width): h (height), w (width), localID(ID), name("Shape"){ID++;}



size_t Shape::ID = 0;

string Shape::getName() const {return name;}

string Shape::getDesc() const {return desc;}

size_t Shape::getID() const {return localID;}

void Shape::setDesc(string tmp){ desc = tmp; }

string Shape::toString(){
	stringstream out;
	out << *this;
	return string(out.str());
}

ostream& operator<<(std::ostream& out, const Shape& shp){
		out<<"Shape info:\n";
		out<<"--------------------\n";
		out<<"Static type: " << typeid(&shp).name()<<'\n';
		out << "Dynamic type: " << typeid(shp).name() << '\n';
		out << "Generic name: " << shp.getName() << "\n";
		out << "Description: " << shp.getDesc() << "\n";
		out << "id: " << shp.getID() << "\n";
		out << "Bounding box width: " << shp.bbw() << "\n";
		out << "Bounding box height: " << shp.bbh() << "\n";
		out << "Scr Area: " << shp.areaScreen() << "\n";
		out<<"Geo Area: " << shp.area() << "\n";
		out<<"Scr Perimeter: " << shp.permScreen() << "\n";
		out << "Geo Perimeter: " << shp.perm() << "\n";

		return out;
}

Shape::~Shape(){};