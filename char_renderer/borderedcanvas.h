#ifndef BORDERED_H
#define BORDERED_H
#include"canvas.h"
#include<vector>
class BorderedCanvas:public Canvas{
public:
	BorderedCanvas(int, int);
	void setChar(int, int, char) override;

	~BorderedCanvas();

};

#endif
