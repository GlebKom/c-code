#pragma once
#include<string>
#include<algorithm>
#include<iostream>

class Command
{
public:
	Command();
	bool parse(std::string&, std::string&, std::string&, std::string&);
	~Command();
};

