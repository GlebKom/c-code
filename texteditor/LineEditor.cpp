#include "LineEditor.h"




LineEditor::LineEditor(std::string file) : cli(std::numeric_limits<size_t>::max()), toWrite(false), file(file){

	std::ifstream fin;

	fin.open(file);

	if (fin.good()){

		
		std::string tempReader;


		while (!fin.eof()){

			getline(fin, tempReader);

			buffer.push_back(tempReader);

		}

		std::cout << "FILE FOUND." << std::endl;


		std::cout << "\"" << file << "\", LINES: " << buffer.size() << std::endl;

		currentLine = buffer.begin();
		cli = 0;
		fin.close();

	}
	else {
		this->file = "?";
		std::cout << "FILE DOES NOT EXIST" << std::endl;
		std::cout << "\"" << file << "\" [NEW FILE]" << std::endl;

	}

	std::cout << "ENTERING COMMAND MODE..." << std::endl;
	//std::cout << ": ";

}


void LineEditor::strToInt(const std::string& source, size_t& dest){

	std::stringstream temp;

	temp << source;

	temp >> dest;

}


void LineEditor::run(){




	while (true) {



		while (!cmd.parse(lho,fcm,rho,scm)){}



		if (!scm.empty())
			std::swap(scm, fcm); // actual command is always kept in first command


		if (fcm == "q")  {
			quit();
			return;
		}

		else if (buffer.empty()){
			onEmpty();
		}

		else if (fcm == "p" || fcm == "n" || fcm == "c") { 
			binary();
			} 


		else if (fcm == "r")  {
			remove();
		 } 
		 else if (fcm == "="){
			 std::cout << cli +1 << '\n';
		 }

		 else if (fcm == "u") {
			 up();
		 } 
		 else if (fcm ==  "d") {
			 down(); 
		 } 
		 else if (fcm == "i" || fcm == "a" ) {
			 //insert();
			 append();
		 }

		 else if (fcm ==  "w")  {
			 write(file);

		 }
		 else{
			 std::cout << "unknown command\n";
		 }


		 // clear everything
		 lhv = 0;
		 rhv = 0;
		 lho.clear();
		 rho.clear();
		 scm.clear();
		 fcm.clear();


		} 

	} 

void LineEditor::onEmpty(){
	if (fcm == "i" || fcm == "a"){


		while (true){
			std::string tmp;
			std::getline(std::cin, tmp);
			if (tmp == "."){
				break;
			}
			else{
				buffer.push_back(tmp);
				currentLine = --buffer.end();

				if (cli == std::numeric_limits<size_t>::max()){
					cli = 0;
				}
				else{
					cli++;
				}

			}
		}

		if (!buffer.empty()){
			toWrite = true;
		}

	}
	else{
	
		std::cout << "error: file empty - enter 'q' to quit, 'a' to append, or'i' to inser\n";

	}
}

void LineEditor::append(){
	toWrite = true;

	if (!rho.empty()){
		std::cout << "error: u not a binary command\n";
		return;
	}

	if ( !parseOperandInsert(lho, lhv) ) return;


	if (!lhv){
		std::cout << "error: zero operand\n";
		return;
	}

	lhv--;


	std::list<std::string> temp;
	std::string tmpstr;

	while (true){
		std::getline(std::cin, tmpstr);
		if (tmpstr == ".") break;
		else{
			temp.push_back(tmpstr);
		}

	}
	
	// nothing has been entered
	if (temp.empty()) return;

	cli = 0;
	currentLine = buffer.begin();

	if (fcm == "a"){
		lhv++; // do not do this for insert
	}

	while (lhv != 0 && currentLine != buffer.end()){
		currentLine++;
		lhv--;
		cli++;
	}

	currentLine = buffer.insert(currentLine, temp.begin(), temp.end());
	std::advance(currentLine, temp.size() - 1);
	cli += temp.size() - 1;


}

void LineEditor::remove(){
	toWrite = true;
	if (rho.empty()){
		rho = lho;
	}



	if (!parseOperand(lho, lhv) || !parseOperand(rho, rhv)) return;


	if (lhv > rhv){
		std::cout << "invalid range: " << lhv << " > " << rhv << '\n';
		return;
	}



	if (!lhv || !rhv){
		std::cout << "error: (one of) operands is zero";
		return;
	}


	--lhv;
	--rhv;

	if (!(lhv < buffer.size())){
		std::cout << "error: starting index is beyond the end\n";
		return;
	}

	auto left = buffer.begin();
	cli = 0;
	rhv++;
	rhv -= lhv;
	
	while (lhv != 0 && left != buffer.end()){
		lhv--;
		left++;
		cli++;
		//std::cout << "cli: " << cli << "\n";
	}

	//rhv++;

	auto right = left;
	//rhv -= lhv;

	while (rhv !=0 && right != buffer.end()){
		rhv--;
		right++;
	}

	currentLine = buffer.erase(left,right);



	if (buffer.empty()){
		cli = std::numeric_limits<size_t>::max();
	}
	else if (currentLine == buffer.end()){
		 currentLine--;
		 cli = buffer.size() - 1;
	}


} 

void LineEditor::down(){


	if (!rho.empty()){
		std::swap(lho, rho);
	}

		if (!parseOperandMove(lho, lhv)){
			return;
		}


		
			if (cli + lhv > buffer.size()) { 

				cli = buffer.size() - 1;
				currentLine = --buffer.end();

				std::cout << "END reached\n";



			}

			else { 



				cli +=  lhv;
				while (lhv--){
					currentLine++;
				}
				//for (size_t i = 0; i < lhv; i++, currentLine++);

			}
		
		

	}

void LineEditor::up(){

	if (!rho.empty()){
		std::swap(lho, rho);
	}

	if (!parseOperandMove(lho, lhv)) { 
		return;
	}


	if (cli < lhv) { //?

		cli = 0;
		currentLine = buffer.begin();

		std::cout << "BOF reached\n";

	}

	else { 

		cli -= lhv;
		while (lhv--){
			currentLine--;
		}

	}
		
		
	}

void LineEditor::binary(){

	if (rho.empty()){
		rho = lho;
	}



	if (!parseOperand(lho, lhv) || !parseOperand(rho, rhv)) return;


	if (lhv > rhv){
		std::cout << "invalid range: " << lhv << " > " << rhv << '\n';
		return;
	}



	if (!lhv || !rhv){  
		std::cout << "error: (one of) operands is zero";
		return;
	}


	--lhv;
	--rhv;
	//std::cout << "here\n";
	if ( !(lhv < buffer.size()) ){
		std::cout << "error: starting index is beyond the end\n";
		return;
	}



	auto left = buffer.begin();

	advance(left, lhv);

	auto right = buffer.begin();


	if (rhv >= buffer.size()) { 
		right = buffer.end();
		currentLine = --buffer.end();
		cli = buffer.size() - 1;

	}
	else {

		advance(right, rhv);
		currentLine = right;
		cli = rhv;
		advance(right, 1);
	}



	if (fcm == "p"){
		print(left, right);
	}
	else if (fcm == "n") {
		printNumbered(lhv, left, right);
	}
	else{
		change(left, right);
	}

}

void LineEditor::quit(){

	if (buffer.empty()){ // if the buffer not empty, than not save it
		return;
	}

	else if (toWrite){

		std::cout << "save changes? Y/N";

		std::string temp;

		while (true){

			std::getline(std::cin, temp);

			if (temp == "Y"){

				write(file);

				return;

			}

			else if (temp == "N"){

				return;

			}
			else {

				std::cout << "error: failed to recognize command\n";

			}
		}
	}

	else return;
}

void LineEditor::write(std::string filename){

	std::string tmp;

	
	if (filename == "?"){
		std::cout << "Enter file name (txt is appended automatically): ";
		std::getline(std::cin, filename);
		filename += ".txt";
	}
	
	std::ofstream out(filename);

	//auto end= buffer.end();

	auto oneToEnd = --buffer.end();

	for (auto it = buffer.begin(); it != oneToEnd; it++){



		out << *it <<'\n';

	}

	out << *oneToEnd;

	toWrite = false;

	out.close();
}

void LineEditor::change(std::list<std::string>::iterator left, std::list<std::string>::iterator right){
	toWrite = true;
	std::string target;

	std::string input;


	std::cout << "change what? ;\n";
	getline(std::cin, target);

	std::cout << "    to what? ;\n";
	getline(std::cin, input);

	std::regex e(target);

	for (auto it = left; it != right; it++){

		*it = std::regex_replace(*it, e, input);

	}
}

void LineEditor::print(std::list<std::string>::iterator left, std::list<std::string>::iterator right){
	for (std::list<std::string>::iterator it = left; it != right; ++it){
		std::cout << *it << '\n';
	}
}

void LineEditor::printNumbered(size_t start, std::list<std::string>::iterator left, std::list<std::string>::iterator right){
	for (std::list<std::string>::iterator it = left; it != right; ++it, start++){
		std::cout << "(" << start + 1 << ") " << *it << std::endl;
	}
}

bool LineEditor::parseOperandInsert(std::string& str, size_t& dest){
	
	if (str.empty()){
		std::stringstream temp;
		std::string result;
		temp << cli + 1;
		temp >> result;
		strToInt(result, dest);
		return true;
	}
	else if (std::all_of(str.begin(), str.end(), isdigit)){
		strToInt(str, dest);
		return true;
	}
	else if (str == "." || str == "$"){
		std::cout << "Use of special characters not allowed for these commands\n";
		return false;
	}
	else{
		std::cout << "Failed to parse operand(s)\n";
		return false;
	}
}

bool LineEditor::parseOperandMove(std::string& str, size_t& dest){
	if (str.empty()){
		dest = 1;
		return true;
	}
	else if (std::all_of(str.begin(), str.end(), isdigit)){
		strToInt(str, dest);
		return true;
	}
	else if (str == "." || str =="$"){
		// std::cout << "Use of special characters not allowed for these commands\n";
		return false;
	}
	else{
		// std::cout << "Failed to parse operand(s)\n";
		return false;
	}

	return false;
}

bool LineEditor::parseOperand(std::string& str, size_t& dest){
	if (str == "." || str.empty()){
		// std::cout << "str == \".\" || str.empty()\n";
		std::stringstream temp;
		std::string result;
		temp << cli + 1;
		temp >> result;
		strToInt(result, dest);
		return true;
	}
	else if (str == "$"){
		// std::cout << "str == \"$\"\n";
		std::stringstream temp;
		std::string result;
		temp << buffer.size();
		temp >> result;
		strToInt(result, dest);

		return true;
	}
	else if (std::all_of(str.begin(), str.end(), isdigit)){
		// std::cout << "std::all_of(str.begin(), str.end(), isdigit)\n";
		strToInt(str, dest);
		return true;
	}
	else{
		// std::cout << "Failed to parse operand(s)\n";
		return false;
	}

	
}

LineEditor::~LineEditor()
{

}