#include "Command.h"


Command::Command()
{
}

bool Command::parse(std::string& lho, std::string& fcm, std::string& rho, std::string& scm){

	std::string in;
	std::cout << ": ";
	std::getline(std::cin, in);
	// std::cout << "\n";
	in.erase(std::remove_if(in.begin(), in.end(), [](char x){return isspace(x) || x == '\t'; }), in.end());
	auto left = in.begin();
	auto right = in.begin();

	// get first operand
	right = std::find_if_not(left, in.end(), [](char x){ return x == '$' || x == '.' || isdigit(x); });
	std::copy(left, right, std::back_inserter(lho));
	left = right;

	// get first command
	right = std::find_if_not(left, in.end(), [](char x){ return isalpha(x) || x == ',' || x == '='; });
	std::copy(left, right, std::back_inserter(fcm));
	left = right;

	// get second operand
	right = std::find_if_not(left, in.end(), [](char x){ return x == '$' || x == '.' || isdigit(x); });
	std::copy(left, right, std::back_inserter(rho));
	left = right;

	//get second command
	right = std::find_if_not(left, in.end(), [](char x){ return isalpha(x) || x == ','; });
	std::copy(left, right, std::back_inserter(scm));
	left = right;

	if (left != in.end()){
		std::cout << "Unrecognised symbol: \n";
		std::cout << in << '\n';
		for (auto it = in.begin(); it != left; ++it){
			std::cout << ' ';
		}
		std::cout << "^\n";
		return false;
	}

	if (!scm.empty() && fcm != ","){
		std::cout << "invalid command separator\n";
		return false;

	}


	// if no arguments are entered
	if (lho.empty() && rho.empty() && fcm.empty() && scm.empty()){
		fcm.append("d");
		lho.append("1");
	}

	//if no second command is provided
	if (!fcm.empty() && !rho.empty() && scm.empty()){
		scm.append("p");
	}

	if (fcm.empty() && rho.empty() && scm.empty()){
		fcm.append("p");
	}



	if (lho.empty() && rho.empty() && fcm == "," && scm.empty()){
		lho.append("1");
		rho.append("$");
		scm.append("p");
	}
	/*
	std::cout << "left hand operand: " << lho << '\n';
	std::cout << "first command: " << fcm << '\n';
	std::cout << "right hand operand: " << rho << '\n';
	std::cout << "second command: " << scm << '\n';
	*/

	return true;

}


Command::~Command()
{
}
