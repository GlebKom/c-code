
#pragma once
#include<list>
#include<string>
#include<fstream>
#include<sstream>
#include<iostream>
#include<regex>
#include<algorithm>
#include<cctype>
#include"Command.h"


class LineEditor
{

	std::list<std::string> buffer; 

	std::list<std::string>::iterator currentLine; 

	Command cmd;


	size_t cli; // current line offset withing the buffer

	bool toWrite; 

	std::string file; // file name

	std::string lho, // left hand operand
		rho; //right hand operand

	size_t lhv; 

	size_t rhv; 

	std::string fcm, //first command
		scm; //second command



	// Commands
	void strToInt(const std::string&, size_t&); 
	void up();
	void down();
	void binary(); // checks several logical commands
	void quit();
	void remove();
	void append();
	void onEmpty();
	void change(std::list<std::string>::iterator left, std::list<std::string>::iterator right);
	void print(std::list<std::string>::iterator left, std::list<std::string>::iterator right);
	void printNumbered(size_t start, std::list<std::string>::iterator left, std::list<std::string>::iterator right);
	bool parseOperand(std::string& source, size_t& dest);
	bool parseOperandMove(std::string& str, size_t& dest);
	bool parseOperandInsert(std::string& str, size_t& dest);
	void write(std::string);
public:
	LineEditor() = delete;
	LineEditor(std::string);

	void run();

	~LineEditor(void);

};
