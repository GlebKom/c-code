#include<iostream>
#include<string>
#include<algorithm>
#include<iterator>
#include<utility>
#include "LineEditor.h"

int main(int argc, char * argv[])
{

	std::string filename;
	switch (argc) {
	case 1: // no file name
		break;
	case 2: // read from argument string
		filename = argv[1];
		break;
	default:
		std::cout << ("too many arguments - all discarded") << std::endl;
		filename.clear();
		break;
	}
	LineEditor ed(filename);
	ed.run();
	return 0;
}

