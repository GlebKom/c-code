#ifndef NODE_H
#define NODE_H

template<typename T>
class List;

#include<ostream>

template<typename T>
class Node {
	friend class List<T>;
	template<typename U>
	friend std::ostream& operator<<(std::ostream&, const List<U>&);
	template<typename U>
	friend class List<U>::iterator;
	T data;
	Node<T>* next;
	Node<T>* prev;
public:
	Node():next{nullptr},prev{nullptr}{}
	Node(T x): next{nullptr}, prev{nullptr}, data{x}{}
	Node(const Node& that): next{nullptr}, prev{nullptr}, data{that.data}{}
	~Node(){}
	Node<T>* getNext(){return next;}
	friend std::ostream& operator<<(std::ostream& out, const Node<T>& nd){
		out << nd.data;
		return out; 
	}
	T* operator->(){return &data;}

	
};
#endif
