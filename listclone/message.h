#ifndef MESSAGE_H
#define MESSAGE_H

#include<string.h>
#include<iostream>
class Message{
	char* msgPtr = nullptr;
	size_t strLen = 0;
	size_t current = 0;
	const size_t MINSIZE = 10; // minimal allocated size
	static const size_t MAX_CAPACITY = 1000; // max possible size of a single line
	void resize(size_t);
	void del();
	void copy(const Message&);
	void copy(const char*);
	public:
	Message();
	Message(const char);
	Message(const char*);
	Message(const Message&);
	Message(Message&&);
	Message& operator=(Message&&);
	Message& operator=(const Message&);
	static size_t max_size();
	void push_back(char);
	void pop_back();
	size_t size() const ;
	bool empty() const ;
	char* c_str();
	friend std::ostream& operator<<(std::ostream&, const Message&); 
	friend std::istream& operator>>(std::istream&, Message&);
	~Message();
};
#endif
