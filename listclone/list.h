#ifndef LIST_H
#define LIST_H

#include "node.h"
#include<cstddef>
#include<iostream>
#include<iterator>
template<typename T>
class List{
private:	
	Node<T>* head;
	Node<T>* tail;
	Node<T>* tmp;
	Node<T>* aux;
	size_t size;
	void del() { // deletes the current list
		aux = nullptr;
		tmp = head;
		while (tmp != nullptr) {
			aux = tmp;
			tmp=tmp->next;
			delete aux;
		}
		aux = tmp  = head = tail = nullptr;
		size = 0;
		
	}

	void copy(const List& that){
		head = new Node<T>();
		tail = new Node<T>();
		head->next = tail;
		tail->prev = head;	
		aux = that.head;
		aux = aux->next;
		while(aux != that.tail){
			push_back(aux->data);
			aux = aux->next;
			size++;
		}
		aux = nullptr;
	}
	
	
	
public:
	List(): size{0}, tmp{nullptr}, head{new Node<T>()}, tail{new Node<T>()} {
		head->next = tail;
		tail->prev = head;
	}

	List(const List& that){
		if(this != &that){
			copy(that);
		}	
	}
	
	List& operator = (const List& that){
		if(this == &that) return *this;
		del();
		copy(that);
		return *this;
	}
	
	virtual	~List(){
		del();
	}
	
	
	void push_front(const T& x){
		tmp = new Node<T>(x);
		tmp->next = head->next;
		tmp->prev = head;
		head->next->prev = tmp;
		head->next = tmp;
		tmp = nullptr;		
		size++;				
	}
	
	T& front(){return head->next->data;}
		
	T& back(){return tail->prev->data;}
	
	void pop_front(){
		tmp = head;
		if (tmp->next == tail){return;}
		tmp = tmp->next;
		head->next = tmp->next;
		tmp->next->prev = head;
		delete tmp;
		size--;
	}
	
	void push_back(const T& x){
		tmp = new Node<T>(x);
		tail->prev->next = tmp;
		tmp->prev = tail->prev;
		tail->prev = tmp;
		tmp->next = tail;
                size++;
        }	

	void pop_back(){
		tmp = tail;
		if (tmp->prev == head){return;}
		tmp = tmp->prev;
		tail->prev = tmp->prev;
		tmp->prev->next = tail;
		delete tmp;
		size--;
	}


	friend std::ostream& operator<<(std::ostream& out, const List<T>& list ){
	
		Node<T>* tmp = list.head->getNext();
		while( tmp != list.tail){
			out << *tmp;
			tmp = tmp->getNext();
		}
		return out;
	}

	class iterator: public std::iterator<std::bidirectional_iterator_tag,
 Node<T>>
	{	
		Node<T>* tmp;
	
		template<typename U>	
		friend	class List;	
	
		public:
		iterator(): tmp{nullptr} {}
		iterator(Node<T> * nd): tmp(nd){}	
		iterator(const iterator& tit) : tmp(tit.tmp) {}
		iterator& operator ++ (){tmp = tmp->next; return *this;}
		iterator operator ++ (int){iterator itr(*this); tmp = tmp->next; return itr;}
		iterator& operator -- (){tmp = tmp->prev; return *this;}
		iterator operator -- (int){iterator itr(*this); tmp = tmp -> prev; return itr;}
		iterator& operator = (const iterator& rhs){tmp = rhs.tmp;
			return *this;}
		bool operator == (const iterator& rhs)
			{return  tmp == rhs.tmp;}
		bool operator != (const iterator& rhs)
			{return tmp != rhs.tmp;}
		T& operator*() {return tmp->data;}
	
		Node<T>& operator->(){return *tmp;}
	};

	iterator insert(iterator it, const T& t){
		Node<T>* tmp = new Node<T>(t);
		it.tmp.next->prev = tmp;
		tmp->next = it.tmp.next;
		tmp->prev = it.tmp; //edited
		it.tmp.next = tmp; 
		size++;
		return iterator(tmp);	
	
	}
	
	template<typename InputIterator>
	iterator insert(iterator it, InputIterator first, InputIterator last){
		Node<T>* tmp;
		List<T> temp;
		// copying the list in the given range
		// this is needed to avoid infinite loops
		for(auto it = first; it != last; ++it){
			temp.push_back(it.tmp.data);
		}
		//inserting
		for(auto itr = temp.begin(); itr != temp.end(); itr++){
			tmp = new Node<T>(itr.tmp); //?
			it.tmp.prev->next = tmp;
			tmp->prev = it.tmp.prev;
			it.tmp.prev = tmp;
			tmp->next = it.tmp;
			temp.pop_front();
			size++;		
		}
		
		return it;
	}

	iterator erase(iterator it){
		Node<T>* tmpPrev = it.tmp->prev;
		Node<T>* tmpNext = it.tmp->next;
	
		tmpPrev->next = tmpNext;
		tmpNext->prev = tmpPrev;
		delete it.tmp; //edited
		size--;
		return iterator(tmpNext); 
		
	}
	
	iterator erase(iterator first, iterator last){
		Node<T>* tmpPrev = first.tmp->prev;
		Node<T>* tmpNext = last.tmp; //edited
	
		tmpPrev->next = tmpNext;
		tmpNext->prev = tmpPrev;
		for(auto it = first; first != last;){
			it = first;
			first++;
			delete it.tmp; //edited
			size--;
		}
		return iterator(tmpNext); 
		
	}
	

	iterator begin(){

		return iterator(head->next); 
	}

	iterator end(){

		return iterator(tail);
	}

	template<typename InputIterator>	
	void print(InputIterator begin, InputIterator end){
		for(auto it = begin; it != end; ++it){
			std::cout << *it<<'\n';
		}	
	}
	

	class reverse_iterator: public std::iterator<std::bidirectional_iterator_tag,
 Node<T>>
	{	private:
		Node<T>* tmp;
		public:
		reverse_iterator(): tmp{nullptr} {}
		reverse_iterator(Node<T> * nd): tmp(nd){}	
		reverse_iterator(const reverse_iterator& tit) : tmp(tit.tmp) {}
		reverse_iterator& operator ++ (){tmp = tmp->prev; return *this;}
		reverse_iterator operator ++ (int){reverse_iterator itr(*this); tmp = tmp->prev; return itr;}
		reverse_iterator& operator -- (){tmp = tmp->next; return *this;}
		reverse_iterator operator -- (int){reverse_iterator itr(*this); tmp = tmp -> next; return itr;}
		reverse_iterator& operator = (const reverse_iterator& rhs){tmp = rhs.tmp;
			return *this;}
		bool operator == (const reverse_iterator& rhs)
			{return  tmp == rhs.tmp;}
		bool operator != (const reverse_iterator& rhs)
			{return tmp != rhs.tmp;}
		T& operator*() {return tmp->data;}
		Node<T>& operator->(){return *tmp;}
		
	};
	

	reverse_iterator rbegin(){

		return reverse_iterator(tail->prev); 
	}

	reverse_iterator rend(){

		return reverse_iterator(head);
	}	
	

};
#endif
