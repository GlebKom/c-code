#include"message.h"


std::ostream& operator<<(std::ostream& out, const Message& msg){
	if(!msg.empty()){
		out << msg.msgPtr;
	}
	return out; 
}

std::istream& operator>>(std::istream& in, Message& msg){
	msg.del();
	char* tmp = new char[msg.MAX_CAPACITY];
	in >> tmp;
	msg.copy(tmp);	
	delete[] tmp;
	return in;
}

char* Message::c_str(){
	return msgPtr;
}


// default constructor
Message::Message(){
	strLen = MINSIZE;
	msgPtr = new char[MINSIZE + 1];
	msgPtr[current = 0] = '\0'; // Does not work on Windows without that	
}

Message::Message(Message&& messageToMove){
	

	msgPtr = messageToMove.msgPtr;

	strLen = messageToMove.strLen;
	current = messageToMove.current;

}

Message& Message::operator=(Message&& messageToMove){


	msgPtr = messageToMove.msgPtr;
	strLen = messageToMove.strLen;
	current = messageToMove.current;

	return *this;

}

Message::Message(char c){
	strLen = MINSIZE;
	msgPtr = new char[MINSIZE+1];
	current = 0;
	msgPtr[current++] = c;
	msgPtr[current] = '\0'; // Does not work on Windows without that
	
}

size_t Message::max_size()   {return MAX_CAPACITY;}

// Three private helper methods begin here.
void Message::resize(size_t max){
//	std::cout<<"Resizing... old size is "<<strLen
//		<<", new size is "<< max<<std::endl;
	char* tmpPtr = new char[max + 1];
	for(size_t i = 0; i < current;++i){
		tmpPtr[i] = msgPtr[i];
	}
	delete[] msgPtr;
	msgPtr = tmpPtr;
	strLen = max;
	
}

void Message::del(){
	delete[] msgPtr;
	msgPtr = nullptr;
	current = strLen = 0;	
}

void Message::copy(const Message& that){
	current = strLen = strlen(that.msgPtr);
	msgPtr = new char[strLen +1];
	strcpy(msgPtr,that.msgPtr);
//	msgPtr[strLen] = '\0';
}

void Message::copy(const char* that){

	current = strLen = strlen(that);
	msgPtr = new char[strLen +1];
	strcpy(msgPtr,that);

}



// copy constructor
Message::Message(const Message& that){
	// self-assignment
	if(this != &that){
		copy(that);
	}
}

Message& Message::operator=(const Message& that){
	if (this == &that) return *this;
	del();
	copy(that);
	return *this; 
}




// c-string constructor
Message::Message(const char* cstring){
	copy(cstring);
}

void Message::push_back(char c){
	// double if the array is full
	if (current == strLen) resize(2 * strLen);
	msgPtr[current++] = c;
	msgPtr[current] = '\0';
	
}

void Message::pop_back(){
	if(!empty()){
		msgPtr[--current] = '\0';
	

		if (current == strLen/4){
			resize(strLen/2);
		}
	}
}


size_t Message::size() const {
	return current;
}

bool Message::empty() const  {
	return current == 0;
}

Message::~Message(){
	del();
}




